<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cheapest extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 private $res_per_page = 5;
	 function __construct()
    {     
        parent::__construct();
      //  $this->load->database();
        $this->load->library(array('session'));
        $this->load->library("jumia");
		$this->load->library('session');
    }
	 
	 
	public function index()
	{
		$this->load->view('main');
	}
	public function search($page_no=1)
	{
		$data = $this->input->post();
		var_dump($data);
		$res = array();
		if(@$data['new_search'] == "yes")
		{
		  $this->session->unset_userdata('search_data'); 
		  $res = $this->jumia->search($data['prd']);
		//  echo "<pre>";
		//  print_r($res);
 //		exit();
          
		   $this->session->set_userdata('search_data',serialize($res)); 
        }
		if($page_no != 1)//avail the filters from the session
		{
			//echo "inside page not 1";
			@$data['contains'] = $this->session->userdata('contains');
			@$data['sl-search'] = "yes";
			@$data['amt'] = $this->session->userdata('amt');
			//print_r($data);
			
			
		}
		$res = unserialize($this->session->userdata('search_data'));//fetch original data then start applying filters
		$min_px =@ min( array_column( $res, 'price' ) );
        $max_px =@ max( array_column( $res, 'price' ) );
		$rep['amt'] = "$min_px - $max_px";
		if(@$data['contains'])
		{
			
			$res = $this->contains($res,'title',trim($data['contains']));	
			$rep['contains'] = $data['contains'];	
			$this->session->set_userdata('contains',trim($data['contains'])); 			
						
		}
		if(@$data['sl-search'] == "yes")//px slider
		{
			$px = $data['amt'];
			$det = explode("-",$px);
			var_dump($det);
			$min = $det[0];
			$max = $det[1];
			$res = $this->price_range($res,'price', trim($min),trim($max));
			$rep['amt'] = $data['amt'];
			$this->session->set_userdata('amt',$data['amt']); 
			
			
		}
		
          

		
		$rep['min_px'] = $min_px;
		$rep['max_px'] = $max_px;
		$rep['page_no'] = $page_no;  
		$total_res = count($res);
		$rep['total_res'] = $total_res;
		$total_pages = ceil($total_res / $this->res_per_page);
		$rep['total_pages'] = $total_pages ;
		var_dump($page_no);echo "dfsdfsd";
		if($page_no == 1)
		{
			$start = 0;
			$end = $this->res_per_page;
		}
		else
		{
			$end = $page_no * $this->res_per_page;
			$start =  $end - $this->res_per_page;
		}
		echo "$start, $end";
		$paginated = array_slice($res, $start, $this->res_per_page);
		echo count($paginated);
		//$rep['data'] = $res;
		$rep['data'] = $paginated;
		$this->session->set_userdata('displayed_data',$res); 
		$this->load->view('results',$rep);
		
		
		//
		
	}
	function single($idx)
	{
		$all_data = unserialize($this->session->userdata('search_data'));
		//var_dump($all_data);
		$item = $this->find_item($all_data, 'idx', $idx);
		
		$data = $item[0];
		$det =  $this->jumia->get_single($item[0]['product_url']);
		$data['desc'] = $det['desc'];
		$data['imgs'] = $det['imgs'];
		$data['brand'] = $det['brand'];
		$rep['item'] = $data;
		var_dump($rep);
		$this->load->view('single',$rep);			
	}
	function contains($array, $key, $val)
    {
	   $c = count($array);
	   $i = 0;
	   $resp = array();
	   while($i < $c)
	   {
		  if(stripos($array[$i][$key], $val) !== false)
		  {
			  $resp[] = $array[$i];

		  }			  
		   
		$i++;   
	   }

       return $resp;
    }
	function find_item($array, $key, $val)
    {
	   $c = count($array);
	   $i = 0;
	   $resp = array();
	   while($i < $c)
	   {
		  if($array[$i][$key] == $val)
		  {
			  $resp[] = $array[$i];

		  }			  
		   
		$i++;   
	   }

       return $resp;
    }
	function price_range($array, $key, $min,$max)
    {
	   $c = count($array);
	   $i = 0;
	   $resp = array();
	   while($i < $c)
	   {
		  if($array[$i][$key] >= $min && $array[$i][$key] <= $max)
		  {
			  $resp[] = $array[$i];
		  }			  
		   
		$i++;   
	   }

       return $resp;
    }
}
