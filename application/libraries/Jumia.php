<?php
require_once 'vendor/autoload.php';
use Symfony\Component\DomCrawler\Crawler;

Class Jumia
{
	private $price_css_name = ".price ";
	private $ratings_css_name = ".total-ratings";
	private $title_css_name = ".title";
	private $img_css_name = "";
	private $product_link_css_name = ".link";
	public function __construct()
    {

       
    }

    function search($search)
    {
		//$crawler = new Crawler($html);
		
		$url = "https://www.jumia.co.ke/catalog/?q=$search";
		//echo htmlspecialchars(file_get_contents($url));exit();
        $crawler = new Crawler(file_get_contents($url));
		$nodes = $crawler->filter('.sku');
	//	echo "<pre>";
		//  print_r($nodes);	exit();
		$total = $nodes->count();
		$res = array();
		$idx = "jm";
		$i = 1;
		foreach($nodes as $node)
		{	
          $det = array();		
		  $item = new Crawler($node);		 
		  try
		  {		 
			  $px = $this->get_att($item,$this->price_css_name);
			  if(strpos($px,"-"))
			  {
				  $tmp = explode("-",$px);
				  $px = $tmp[0];
			  }
			  $det['price'] = trim($px,"KSh");
			  
			  $det['price'] = str_replace(",","",$det['price']);
			  $det['ratings'] = trim($this->get_att($item,$this->ratings_css_name),"()");
			  $det['title'] = $this->get_att($item,$this->title_css_name);
			  $img = $this->get_img($item);
			  if($img === false)
			  {
				  $img = $this->get_img_alt($item);
			  }	  
			  $det['img_url'] = $img;
			  $det['product_url'] = $this->get_prd_url($item,$this->product_link_css_name);		
			  $det['site'] = "Jumia";
			  $det['idx'] = $idx.$i;
			  $i++;
			  if(strlen($det['title']) > 0)
			  {
				if(count($res) == 40)
				{
					break;
				}
			    $res[] = $det;	
			  }				
		  }
		  catch(Exception $e) 
		  {
			echo '';
		  }
		} 
			
        return $res;
    }
	function get_att($item,$css_class)
	{
	    try
		{
			$rep = $item->filter($css_class)->eq(0)->text();
			
			
			return $rep;
		}
		catch(Exception $e)
		{
		   return '';	
		}
	}
	function get_prd_url($item,$css_class)
	{
		try
		{
			return $item->filter($css_class)->attr('href');
		  	
		}
		catch(Exception $e)
		{
			return false;			
		}		
	}
	function get_img($item)
	{
		try
		{
			return $item->filter('noscript > img')->attr('src');
		  	
		}
		catch(Exception $e)
		{
			return false;			
		}		
	}
	function get_img_alt($item)
	{
		try
		{
		  	return $item->filter(".link ")->eq(0)->text();
		}
		catch(Exception $e)
		{
			return " ";			
		}
		
	}
	
	function get_price($item)
	{
		try
		{
			$item->filter($price_css_name)->eq(0)->text();
		}
		catch(Exception $e)
		{
			
		}
		
		
	}
	function get_single($url)
	{
		$crawler = new Crawler(file_get_contents($url));
		$brand = $crawler->filterXpath("//div[@class='-fs14 -pvxs']/a")->text("");
		$data = $crawler->filterXpath("//meta[@property='og:description']")->extract(array('content'));
		$nodes = $crawler->filter(".itm");
		$images = array();
		$resp = array();
		foreach($nodes as $node)
		{
			//echo "<hr>";
			//echo "in nodes";
			$cr = new Crawler($node);	
			try
			{			
				//$image = $cr->filter('label>img')->extract(['_name', '_text', 'class']);	
			    $image = $cr->filter('label>img')->attr('data-src');	
				//var_dump($image);
				$images[] = $image;
			}
			catch(Exception $e)
			{
	
			}
			
			
		//	var_dump($cr);
	//	exit();
			
		}
		$resp['desc'] = $data[0];
		$resp['imgs'] = $images;
		$resp['brand'] = $brand;
		return $resp;
		
		
		
	}
	
	
	
	
	/*  echo $item->filter('.total-ratings')->eq(0)->text();
			  echo $item->filter('.title')->eq(0)->text();
			  echo $item->filter('.price')->eq(0)->text();
			  //echo $item->filter('.link ')->eq(0)->attr('href');
			   echo $item->filter('.link')->eq(0)->attr('href'); */
	
	
}




?>