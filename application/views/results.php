<?php
/* echo "<pre>";
print_r($data);exit; */
?>
<!DOCTYPE html>

<html lang="en">
<head>
<title>Resale_v2 a Classified ads Category Flat Bootstrap Responsive Website Template | Cars :: w3layouts</title>
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css"><!-- bootstrap-CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-select.css"><!-- bootstrap-select-CSS -->
<link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery-ui1.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" /><!-- fontawesome-CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>css/menu_sideslide.css" type="text/css" media="all"><!-- Navigation-CSS -->
<!-- meta tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //meta tags -->
<!--fonts--
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!--//fonts-->	
<!-- js -->
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
<!-- js -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url();?>js/bootstrap.js"></script>
<script src="<?php echo base_url();?>js/bootstrap-select.js"></script>
<script>

  $(document).ready(function () {
    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>
<!-- language-select -->
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.leanModal.min.js"></script>
<link href="<?php echo base_url();?>css/jquery.uls.css" rel="stylesheet"/>
<link href="<?php echo base_url();?>css/jquery.uls.grid.css" rel="stylesheet"/>
<link href="<?php echo base_url();?>css/jquery.uls.lcd.css" rel="stylesheet"/>
<!-- Source -->
<script src="<?php echo base_url();?>js/jquery.uls.data.js"></script>
<script src="<?php echo base_url();?>js/jquery.uls.data.utils.js"></script>
<script src="<?php echo base_url();?>js/jquery.uls.lcd.js"></script>
<script src="<?php echo base_url();?>js/jquery.uls.languagefilter.js"></script>
<script src="<?php echo base_url();?>js/jquery.uls.regionfilter.js"></script>
<script src="<?php echo base_url();?>js/jquery.uls.core.js"></script>
<script>
			$( document ).ready( function() {
				$( '.uls-trigger' ).uls( {
					onSelect : function( language ) {
						var languageName = $.uls.data.getAutonym( language );
						$( '.uls-trigger' ).text( languageName );
					},
					quickList: ['en', 'hi', 'he', 'ml', 'ta', 'fr'] //FIXME
				} );
			} );
		</script>
<!-- //language-select -->
<!-- switcher-grid and list alignment -->
<script src="<?php echo base_url();?>js/tabs.js"></script>	
<script type="text/javascript">
$(document).ready(function () {    
var elem=$('#container ul');      
	$('#viewcontrols a').on('click',function(e) {
		if ($(this).hasClass('gridview')) {
			elem.fadeOut(1000, function () {
				$('#container ul').removeClass('list').addClass('grid');
				$('#viewcontrols').removeClass('view-controls-list').addClass('view-controls-grid');
				$('#viewcontrols .gridview').addClass('active');
				$('#viewcontrols .listview').removeClass('active');
				elem.fadeIn(1000);
			});						
		}
		else if($(this).hasClass('listview')) {
			elem.fadeOut(1000, function () {
				$('#container ul').removeClass('grid').addClass('list');
				$('#viewcontrols').removeClass('view-controls-grid').addClass('view-controls-list');
				$('#viewcontrols .gridview').removeClass('active');
				$('#viewcontrols .listview').addClass('active');
				elem.fadeIn(1000);
			});									
		}
	});
});
</script>
<!-- //switcher-grid and list alignment -->
</head>
<body>
	<!-- Navigation -->
		<div class="agiletopbar">
			<div class="wthreenavigation">
				<div class="menu-wrap">
				<nav class="menu">
					<div class="icon-list">
						<a href="mobiles.html"><i class="fa fa-fw fa-mobile"></i><span>Mobiles</span></a>
						<a href="electronics-appliances.html"><i class="fa fa-fw fa-laptop"></i><span>Electronics and appliances</span></a>
						<a href="cars.html"><i class="fa fa-fw fa-car"></i><span>Cars</span></a>
						<a href="bikes.html"><i class="fa fa-fw fa-motorcycle"></i><span>Bikes</span></a>
						<a href="furnitures.html"><i class="fa fa-fw fa-wheelchair"></i><span>Furnitures</span></a>
						<a href="pets.html"><i class="fa fa-fw fa-paw"></i><span>Pets</span></a>
						<a href="books-sports-hobbies.html"><i class="fa fa-fw fa-book"></i><span>Books, Sports & Hobbies</span></a>
						<a href="fashion.html"><i class="fa fa-fw fa-asterisk"></i><span>Fashion</span></a>
						<a href="kids.html"><i class="fa fa-fw fa-asterisk"></i><span>Kids</span></a>
						<a href="services.html"><i class="fa fa-fw fa-shield"></i><span>Services</span></a>
						<a href="jobs.html"><i class="fa fa-fw fa-at"></i><span>Jobs</span></a>
						<a href="real-estate.html"><i class="fa fa-fw fa-home"></i><span>Real Estate</span></a>
					</div>
				</nav>
				<button class="close-button" id="close-button">Close Menu</button>
			</div>
			<button class="menu-button" id="open-button"> </button>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- //Navigation -->
	<!-- header -->
	<header>
		
		<div class="container" style="border:1px solid yellow;">
			<div class="agile-its-header">
				<div class="logo">
					<h1><a href="index.html"><span>Cheap</span>est</a></h1>
				</div>
				<div class="agileits_search">
					<form action="<?php echo site_url("Cheapest/search"); ?>" method="post">
						<input name="prd" type="text" placeholder="What are you looking for?" required="true" />
						<input type="hidden" name="new_search" value="yes">
						<input type="submit" class="btn btn-default" name="search" value="Search" aria-label="Left Align">
							
					</form>
				
				</div>	
				<div class="clearfix"></div>
			</div>
		</div>
	</header>
	<!-- //header -->
	<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs">
			<a href="index.html"><i class="fa fa-home home_1"></i></a> 
		</div>
	</div>
	<!-- //breadcrumbs -->
	<!-- Cars -->
	<div class="total-ads main-grid-border">
		<div class="container" >
			<div class="select-box" >
				
				
				ad here
				<div class="clearfix"></div>
			</div>
			<div class="ads-grid">
				<div class="side-bar col-md-3">
					<div class="search-hotel">
					<h3 class="agileits-sear-head">Name contains</h3>
					<form action="<?php echo site_url("Cheapest/search") ;?>" method="post">
						<input type="text" value="<?php echo @$contains;?>" placeholder="Product name..."  name="contains">
						<input type="hidden" name="amt" value="<?php echo @$amt;?>">
						<input type="hidden"  name="sl-search" value="yes">
						<input type="submit" value=" ">
					</form>
				</div>
				
				<div class="range">
					<h3 class="agileits-sear-head">Price range KShs. </h3>
							<ul class="dropdown-menu6">
								<li>
									<form id="px-slider" action="<?php echo site_url("Cheapest/search") ;?>" method="post">              
									  <div id="slider-range"></div>							
										<input type="text" value="<?php echo @$amt;?>" name="amt" id="amount" style="border: 0; color: #ffffff; font-weight: normal;" />
										<input type="hidden" name="contains" value="<?php echo @$contains;?>">
										<input type="hidden"  name="sl-search" value="yes">
									  </li>	
									</form>
							</ul>
							<!---->
							
							<script type="text/javascript" src="<?php echo base_url();?>js/jquery-ui.js"></script>
							<script type='text/javascript'>//<![CDATA[ 
							var min_p = "<?php echo $min_px;?>";
							var max_p = "<?php echo $max_px;?>";	
							var amt = document.getElementById("amount").value;
							var prices = amt.split("-");
							var p_min = parseInt(prices[0].trim());
							var p_max = parseInt(prices[1].trim());
							//alert(min_p);
							//alert(max_p);
							$(function() {
								$( "#slider-range" ).slider({
									range: true,
									/*  min: 10,
									max: 20,  */
									step: 1,
								/*	values: [ min_p, max_p ],*/
									slide: function( event, ui ) {
										$( "#amount" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
										//document.getElementById("px-slider").submit();
									},
									stop:function( event, ui ) {
										document.getElementById("px-slider").submit();
									}
								});
								$( "#amount" ).val( "" + p_min + " - " + p_max );
								$( "#slider-range" ).slider( "option", "max", parseInt(max_p) );
								$( "#slider-range" ).slider( "option", "min", parseInt(min_p) );
								$( "#slider-range" ).slider( "option", "values", [ parseInt(p_min), parseInt(p_max) ] );
									
});
							</script>
							
				</div>
				<div class="w3-brand-select">
					<h3 class="agileits-sear-head">Brand name</h3>
					  <select class="selectpicker" data-live-search="true">
					  <option data-tokens="All">All</option>
					  <option data-tokens="Audi">Audi</option>
					  <option data-tokens="Bentley">Bentley</option>
					  <option data-tokens="BMW">BMW</option>
					  <option data-tokens="Chevrolet">Chevrolet</option>
					  <option data-tokens="Ferrari">Ferrari</option>
					  <option data-tokens="Fiat">Fiat</option>
					  <option data-tokens="Force Motors">Force Motors</option>
					  <option data-tokens="Ford">Ford</option>
					  <option data-tokens="Hindustan Motors">Hindustan Motors</option>
					  <option data-tokens="Honda">Honda</option>
					  <option data-tokens="Hyundai">Hyundai</option>
					  <option data-tokens="Isuzu">Isuzu</option>
					  <option data-tokens="Jaguar">Jaguar</option>
					  <option data-tokens="Lamborghini">Lamborghini</option>
					  <option data-tokens="Landrover">Landrover</option>
					  <option data-tokens="Other Brands">Other Brands</option>
					</select>
				</div>
				<div class="w3ls-featured-ads" style="border:2px solid blue;">
					<h2 class="sear-head fer">Featured Ads</h2>
					<div class="w3l-featured-ad">
						<a href="single.html">
							<div class="w3-featured-ad-left">
								<img src="images/f1.jpg" title="ad image" alt="" />
							</div>
							<div class="w3-featured-ad-right">
								<h4>Lorem Ipsum is simply dummy text of the printing industry</h4>
								<p>$ 450</p>
							</div>
							<div class="clearfix"></div>
						</a>
					</div>
					<div class="w3l-featured-ad">
						<a href="single.html">
							<div class="w3-featured-ad-left">
								<img src="images/f2.jpg" title="ad image" alt="" />
							</div>
							<div class="w3-featured-ad-right">
								<h4>Lorem Ipsum is simply dummy text of the printing industry</h4>
								<p>$ 380</p>
							</div>
							<div class="clearfix"></div>
						</a>
					</div>
					<div class="w3l-featured-ad">
						<a href="single.html">
							<div class="w3-featured-ad-left">
								<img src="images/f3.jpg" title="ad image" alt="" />
							</div>
							<div class="w3-featured-ad-right">
								<h4>Lorem Ipsum is simply dummy text of the printing industry</h4>
								<p>$ 560</p>
							</div>
							<div class="clearfix"></div>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
				</div>
				<div class="agileinfo-ads-display col-md-9">
					<div class="wrapper">					
					<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					  <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
						<li role="presentation" class="active">
						  <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">
							<span class="text">Search Results</span>
						  </a>
						</li>
						
					  </ul>
					  <div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
						   <div>
												<div id="container">
								<div class="view-controls-list" id="viewcontrols">
									<label>view :</label>
									<a class="gridview"><i class="glyphicon glyphicon-th"></i></a>
									<a class="listview active"><i class="glyphicon glyphicon-th-list"></i></a>
								</div>
								<div class="sort">
								   <div class="sort-by">
										<label>Sort By : </label>
										<select>
														<option value="">Most recent</option>
														<option value="">Price: KShs Low to High</option>
														<option value="">Price: KShs High to Low</option>
										</select>
									   </div>
									 </div>
								<div class="clearfix"></div>
								<?php
									$c = count($data);
									if($c < 1)
									{
										echo "<h3>No results were found</h3>";
									}
									else
									{
										?>
										<ul class="list">
										<?php
															
										$i = 0;
										while($i < $c)
										{
											//echo $data[$i]['product_url'];
											?>
											<a href="<?php echo site_url("Cheapest/single/".$data[$i]['idx']);?>">
											<!--<a href="<?php echo site_url("Cheapest/single/1");?>">-->
												<li>
												<img src="<?php echo $data[$i]['img_url'];?>" title="" alt="" />
												<section class="list-left" >
												<h5 class="title"><?php echo $data[$i]['title'];?></h5>
												<span class="adprice">KShs. <?php echo @number_format($data[$i]['price'],2);?></span>
												<p class="catpath">Total ratings: <?php echo $data[$i]['ratings'];?></p>
												
												</section>
												<?php 
												if($data[$i]['site'] == "Jumia")
												{
												   $logo_url = base_url()."images/jumia-logo.png";	
												}
												?>
												<section class="list-right" style="
												background-image: url('<?php echo $logo_url;?>');
												background-size: 100% 100%;
												background-repeat: no-repeat;">
												<!--<span class="date">Today, 17:55</span>-->
												<span class="date">
												
												
												
												</section>
												<div class="clearfix"></div>
												</li> 
											</a>
										<?php	
											$i++;
										}
									}
							?>
							</ul>
						</div>
							</div>
						</div>
						
						
							</div>
						</div>
						
						<?php
						if($total_pages > 1)
						{
							echo '<ul class="pagination pagination-sm">';
							echo '<li><a href="#">Prev</a></li>';
							$i = 1;
							while($i <= $total_pages)
							{
								echo '<li><a href='.site_url("Cheapest/search/$i").'>'.$i.'</a></li>';
								$i++;
							}
							echo '<li><a href="#">Next</a></li>';							
							echo '</ul>';
						}
							
						?>								
						
					  </div>
					</div>
				</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>	
	</div>
	<!-- // Cars -->
	<!--footer section start-->		
		<footer>
			<div class="w3-agileits-footer-top">
				<div class="container">
					<div class="wthree-foo-grids">
						<div class="col-md-3 wthree-footer-grid">
							<h4 class="footer-head">Who We Are</h4>
							<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
							<p>The point of using Lorem Ipsum is that it has a more-or-less normal letters, as opposed to using 'Content here.</p>
						</div>
						<div class="col-md-3 wthree-footer-grid">
							<h4 class="footer-head">Help</h4>
							<ul>
								<li><a href="howitworks.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>How it Works</a></li>						
								<li><a href="sitemap.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Sitemap</a></li>
								<li><a href="faq.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Faq</a></li>
								<li><a href="feedback.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Feedback</a></li>
								<li><a href="contact.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Contact</a></li>
								<li><a href="typography.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Short codes</a></li>
								<li><a href="icons.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Icons Page</a></li>
							</ul>
						</div>
						<div class="col-md-3 wthree-footer-grid">
							<h4 class="footer-head">Information</h4>
							<ul>
								<li><a href="regions.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Locations Map</a></li>	
								<li><a href="terms.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Terms of Use</a></li>
								<li><a href="popular-search.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Popular searches</a></li>	
								<li><a href="privacy.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Privacy Policy</a></li>	
							</ul>
						</div>
						<div class="col-md-3 wthree-footer-grid">
							<h4 class="footer-head">Contact Us</h4>
							<span class="hq">Our headquarters</span>
							<address>
								<ul class="location">
									<li><span class="glyphicon glyphicon-map-marker"></span></li>
									<li>CENTER FOR FINANCIAL ASSISTANCE TO DEPOSED NIGERIAN ROYALTY</li>
								</ul>	
								<div class="clearfix"> </div>
								<ul class="location">
									<li><span class="glyphicon glyphicon-earphone"></span></li>
									<li>+0 561 111 235</li>
								</ul>	
								<div class="clearfix"> </div>
								<ul class="location">
									<li><span class="glyphicon glyphicon-envelope"></span></li>
									<li><a href="mailto:info@example.com">mail@example.com</a></li>
								</ul>						
							</address>
						</div>
						<div class="clearfix"></div>
					</div>						
				</div>	
			</div>	
			<div class="agileits-footer-bottom text-center">
			<div class="container">
				<div class="w3-footer-logo">
					<h1><a href="index.html"><span>Re</span>sale-v2</a></h1>
				</div>
				<div class="w3-footer-social-icons">
					<ul>
						<li><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></a></li>
						<li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></a></li>
						<li><a class="flickr" href="#"><i class="fa fa-flickr" aria-hidden="true"></i><span>Flickr</span></a></li>
						<li><a class="googleplus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i><span>Google+</span></a></li>
						<li><a class="dribbble" href="#"><i class="fa fa-dribbble" aria-hidden="true"></i><span>Dribbble</span></a></li>
					</ul>
				</div>
				<div class="copyrights">
					<p> © 2016 Resale. All Rights Reserved | Design by  <a href="http://w3layouts.com/"> W3layouts</a></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		</footer>
        <!--footer section end-->
</body>
		<!-- Navigation-JavaScript -->
			<script src="<?php echo base_url();?>js/classie.js"></script>
			<script src="<?php echo base_url();?>js/main.js"></script>
		<!-- //Navigation-JavaScript -->
		<!-- here stars scrolling icon -->
			<script type="text/javascript">
				$(document).ready(function() {
					/*
						var defaults = {
						containerID: 'toTop', // fading element id
						containerHoverID: 'toTopHover', // fading element hover id
						scrollSpeed: 1200,
						easingType: 'linear' 
						};
					*/
										
					$().UItoTop({ easingType: 'easeOutQuart' });
										
					});
			</script>
			<!-- start-smoth-scrolling -->
			<script type="text/javascript" src="<?php echo base_url();?>js/move-top.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>js/easing.js"></script>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
					});
				});
			</script>
			<!-- start-smoth-scrolling -->
		<!-- //here ends scrolling icon -->
</html>